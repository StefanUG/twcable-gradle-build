package com.valtech.cq.slingmodelstest;

/**
 * A simple service interface
 */
interface HelloService {
    
    /**
     * @return the name of the underlying JCR repository implementation
     */
    String getRepositoryName();

}