package com.valtech.cq.slingmodelstest;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.day.cq.wcm.api.Page;

@Model(adaptables = Resource , defaultInjectionStrategy = DefaultInjectionStrategy.REQUIRED)
class HelloBean {

	@Inject
	HelloService service;
	
	@Inject
	Page resourcePage;
	
	@Inject
	String greeting;
	
	@Inject
	ToolbarConfig toolbar;

	String getRepositoryName() {
		return service.getRepositoryName();
	}
	
	String getPageTitle() {
		return resourcePage.getTitle();
	}
	
	String getStaticGreeting() {
		return "Hello world!";
	}

	List<String> getLanguages() {
		resourcePage.getAbsoluteParent(1).listChildren().collect {
			it.title
		}
	}
	
}
