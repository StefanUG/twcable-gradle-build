package com.valtech.cq.slingmodelstest.impl;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.spi.DisposalCallbackRegistry;
import org.apache.sling.models.spi.Injector;

import com.valtech.cq.slingmodelstest.ToolbarConfig;


@Component
@Service
@Property(name = "service.ranking", intValue = 50)
class SiteConfigInjector implements Injector {

	String getName() {
		"site-config";
	}

	Object getValue(Object adaptable, String name, Type declaredType, AnnotatedElement element, DisposalCallbackRegistry callbackRegistry) {
		if (declaredType == ToolbarConfig) {
			if (adaptable instanceof Resource) {
				Resource r = adaptable;
				
				String configPath = "${ getAbsoluteParent(r.path, 2) }/toolbar/jcr:content"
				
				Resource toolbar = r.resourceResolver.getResource(configPath)
				return toolbar.adaptTo( ToolbarConfig )
			}
		}
	}

	
	static String getAbsoluteParent(String path, int level) {
		int idx = 0;
		int len = path.length();
		while ((level >= 0) && (idx < len)) {
			idx = path.indexOf(47, idx + 1);
			if (idx < 0) {
				idx = len;
			}
			--level;
		}
		return ((level >= 0) ? "" : path.substring(0, idx));
	}	
	
	
}
