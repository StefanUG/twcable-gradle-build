package com.valtech.cq.slingmodelstest;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables= Resource , defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
interface ToolbarConfig {

	@Inject @Named("jcr:title")
	String getTitle();
	
	@Inject
	String getSubtitle();
	
}
